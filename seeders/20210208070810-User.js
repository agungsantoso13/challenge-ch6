'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users',
      [
        {
          user_name: "nama1",
          password: "12345",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          user_name: "nama2",
          password: "12345",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          user_name: "nama3",
          password: "12345",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {}
    );
  },


  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {
      truncate: true,
      restartIdentity: true
    });
  }
};

