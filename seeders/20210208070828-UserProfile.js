'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserProfiles',
      [
        {
          first_name: "Kesatu",
          last_name: "Nomersatu",
          gender: "laki-laki",
          email: "kesatu1@email.com",
          userId: "3",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          first_name: "Kedua",
          last_name: "Nomerdua",
          gender: "laki-laki",
          email: "kedua2@email.com",
          userId: "1",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          first_name: "Ketiga",
          last_name: "Nomortiga",
          gender: "perempuan",
          email: "ketiga3@email.com",
          userId: "2",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {});

  },


  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserProfiles', null, {
      truncate: true,
      restartIdentity: true
    });
  }
};