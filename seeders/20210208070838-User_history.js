'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('User_histories',
      [{
        log: "5-2",
        userId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        log: "3-2",
        userId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        log: "2-5",
        userId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      }
      ], {});

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User_histories', null, {
      truncate: true,
      restartIdentity: true
    });
  }
};