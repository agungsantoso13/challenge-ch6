const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');

module.exports = {
    dashboard: (req, res) => {
        if (req.user) {
            res.status(200).render('dashboard.ejs', { admin: req.user });
        } else {
            res.status(400).render('user/login', {
                message: 'Please login to continue',
                messageClass: 'alert-danger'
            });
        }
    },
    clear: function (req, res) {
        cookie = req.cookies;
        for (var prop in cookie) {
            if (!cookie.hasOwnProperty(prop)) {
                continue;
            }
            res.cookie(prop, '', { expires: new Date(0) });
            console.log('Admin user logout'); //tambahan untuk pengecekan karena message belum bisa
        }
        res.status(300).redirect('/login');
    }
}
