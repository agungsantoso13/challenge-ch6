const express = require('express');
const app = express();
const db = require('../models/index');
const { User_history } = require('../models');


module.exports = {
    list: (req, res) => {
        User_history.findAll({
            order: [
                ["id", "ASC"]
            ],
            include: {
                model: db.User
            }
        }).then(historyall => {
            // res.send(historyall)
            res.status(200).render('user/show-history.ejs', { table: historyall })
        }).catch(err => {
            res.status(400).json({
                message: `Halaman view history All gak ketemu ${err}`
            })
        })
    },
    create: (req, res) => {
        User_history.create({
            log: req.body.log,
            userId: req.body.userId
        }).then(newhistory => {
            res.status(300).redirect('/user/history')
        }).catch(err => {
            res.status(400).json({
                message: `Halaman redirect history gak ketemu ${err}`
            })
        })
    },
    delete: (req, res) => {
        User.destroy({ where: { id: req.params.id } })
            .then(userdelete => {
                res.status(201).redirect('/user')
            })
    }
}