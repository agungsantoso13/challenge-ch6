const express = require('express');
const app = express();
const db = require('../models/index');
const { User } = require('../models');

//belum error handling

module.exports = {
    list: (req, res) => {
        User.findAll({
            order: [
                ["id", "ASC"]
            ],
            include: [
                {
                    model: db.UserProfile
                },
                {
                    model: db.User_history
                },
            ]
        })
            .then(userall => {
                // res.send(userall)
                res.status(200).render('user/index.ejs', { table: userall })
            })
            .catch(err => {
                res.status(400).json({
                    message: `Halaman view user All error  :${err}`
                })
            })
    },
    getById: (req, res) => {
        User.findOne({
            where: { id: req.params.id }
        })
            .then(userId => {
                res.status(200).render('user/show', { userId })
            })
            .catch(err => {
                res.status(400).json({
                    message: "Get user by ID tidak ada"
                })
            })
    },
    create: (req, res) => {
        User.create({
            user_name: req.body.user_name,
            password: req.body.password
        }).then(newUser => {
            res.status(300).redirect('/user/profile/add')
        }).catch(err => {
            res.status(400).json({
                message: "Halaman redirect dari user gak ketemu"
            })
        })
    },
    update: (req, res) => {
        User.update({
            user_name: req.body.user_name,
            password: req.body.password
        }, {
            where: { id: req.params.id }
        })
            .then(user => {
                res.status(300).redirect('/user')
            }).catch(err => {
                res.status(400).json({
                    message: `Update user error : ${err}`
                })
            })
    },
    delete: (req, res) => {
        User.destroy({ where: { id: req.params.id } })
            .then(userdelete => {
                res.status(201).redirect('/user')
            }).catch(err => {
                res.status(400).json({
                    message: `delete user error : ${err}`
                })
            })
    },
    formCreate: (req, res) => {
        res.status(200).render('user/register-user.ejs')
    },
    updateById: (req, res) => {
        User.findByPk(req.params.id)
            .then(userupdate => {
                if (userupdate) {
                    res.status(200).render('user/update-user.ejs', { user: userupdate })
                } else {
                    res.status(400).json({
                        message: "User is Not Found"
                    })
                }
            }).catch(err => {
                res.status(400).json({
                    message: "gak masuk boss"
                })
            })
    }
}
