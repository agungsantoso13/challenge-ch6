const express = require('express');
const app = express();
const db = require('../models/index');
const { UserProfile } = require('../models');


//belum error handling

module.exports = {
    list: (req, res) => {
        UserProfile.findAll({
            order: [
                ["id", "ASC"]
            ],
            include: {
                model: db.User
            }
        }).then(profileall => {
            // res.send(profileall)
            res.status(200).render('user/show-profile.ejs', { table: profileall })
        }).catch(err => {
            res.status(400).json({
                message: `Halaman view user profile problem: ${err}`
            })
        })
    },
    getById: (req, res) => {
        UserProfile.findOne({
            where: { id: req.params.id }
        })
            .then(profileId => {
                res.status(200).render('user/detail-profile.ejs', { profileId })
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman view profile by Id gak ketemu"
                })
            })
    },
    create: (req, res) => {
        const { first_name, last_name, gender, email } = req.body;
        UserProfile.create(
            { first_name, last_name, gender, email }
        ).then(newProfile => {
            res.status(300).redirect('/user')
        }).catch(err => {
            res.status(400).json({
                message: `Buat profile tidak berhasil ${err}`
            })
        })
    },
    update: (req, res) => {
        const { first_name, last_name, gender, email } = req.body;
        UserProfile.update({ first_name, last_name, gender, email },
            {
                where: { id: req.params.id }
            })
            .then(profileupdate => {
                // res.send(profileupdate)
                res.status(300).redirect('/user')
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman view profile by Id gak ketemu"
                })
            })
    },
    delete: (req, res) => {
        UserProfile.destroy({ where: { id: req.params.id } })
            .then(profiledelete => {
                res.status(300).redirect('/user')
            })
            .catch(err => {
                res.status(400).json({
                    message: "Halaman delete profile by Id gak ketemu"
                })
            })
    },
    formCreate: (req, res) => {
        res.render('user/register-profile.ejs')
    }
    , updateById: (req, res) => {
        UserProfile.findByPk(req.params.id)
            .then(profileupdate => {
                if (profileupdate) {
                    res.status(200).render('user/update-profile.ejs', { profileupdate })
                } else {
                    res.status(400).json({
                        message: "User profile is Not Found"
                    })
                }
            }).catch(err => {
                res.status(400).json({
                    message: "gak masuk boss"
                })
            })
    }
}
