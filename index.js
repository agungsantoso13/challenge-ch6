const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const crypto = require('crypto');
const methodOverride = require('method-override');

const port = process.env.PORT | 3000;
const db = require('./models');


//import controller & router
const authController = require('./controller/auth');
const loginRouter = require('./routes/login-logout-router');
const userRouter = require('./routes/user-router');
const userprofileRouter = require('./routes/user-profile-router');
const userhistoryRouter = require('./routes/user-history-router');


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'))
app.use(cookieParser());

//static file
app.use(express.static('public'));

//template Engine
app.set("view engine", "ejs");



//middleware authentifikasi
const authTokens = {};
app.use((req, res, next) => {
    const authToken = req.cookies['AuthToken'];
    req.user = authTokens[authToken];
    next();
});
// authentifikasi TOKEN
const generateAuthToken = () => {
    return crypto.randomBytes(30).toString('hex');
}

// dummy akun statis, super admin 
const user = [
    {
        first_Name: 'John',
        last_Name: 'Doe',
        user_name: 'johndoe',
        password: '12345'
    }
];
// POST login untuk autentifikasi
app.post('/login', (async (req, res) => {
    const { user_name, password } = req.body;

    const userAdmin = user.find(u => {
        return u.user_name === user_name && password === u.password
    });

    //login usergame
    let userGame = await db.User.findOne({ where: { user_name: user_name, password: password } })
    if (userGame === null) {
        //login super admin
        if (userAdmin) {
            const authToken = generateAuthToken();

            // Store authentication token
            authTokens[authToken] = userAdmin;

            // Setting the auth token in cookies
            res.cookie('AuthToken', authToken);

            // Redirect user to the protected page
            res.status(300).redirect('/dashboard');
        } else {
            res.status(400).render('user/login', {
                message: 'Invalid username or password',
                messageClass: 'alert-danger'
            });
        }
    } else {
        res.send('masuk ke aplikasi game boss')
    }
}));


/*route start here*/
// Main call to action
app.get('/', (req, res) => {
    res.render('pages/landing');
    res.status(200);
});

//endpoint login
app.use('/login', loginRouter);
//endpoint untuk logout (clear cookie value)
app.get('/logout', authController.clear)
//endpoint dashboard super admin (protected by cookies)
app.get('/dashboard', authController.dashboard);
//endpoint untuk user
app.use('/user', userRouter);
app.use('/user/profile', userprofileRouter);
app.use('/user/history', userhistoryRouter);


// 404 Handler
app.use((req, res, next) => {
    const error = new Error("Page not found Boss!")
    error.status = 404;
    res.render("error404.ejs")
    next(error)
})
// internal server error
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.render("error500.ejs", { error });
})


//listening
app.listen(port, () => {
    console.log(`server nyala di port ${port} bos!`)
})